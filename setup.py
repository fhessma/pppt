# setup.py for pppt

from distutils.core import setup, Extension
from distutils.sysconfig import get_python_lib
import os

# get path to numpy headers
numpy_dir = os.path.join(get_python_lib(plat_specific=1), 'numpy/core/include/')

setup(
    name='pppt',
    version='0.2',
    ext_modules=[],
    url='',
    license='',
    author='Frederic V. Hessman',
    author_email='hessman@astro.physik.uni-goettingen.de',
    description='pytel primitive planning tool',
    requires=['astroquery','tkinter','pytkgen','tkcalendar','tkintertable','yaml']
)

