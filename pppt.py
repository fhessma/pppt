#! python

"""
Simple observation planner for pyobs.

All particular information about a pyobs site is stored in the JSON and YAML files.

The information is stored in JSON. The user can maintain a set of templates with
values that replace those in the various fields.

Stupid tkinter requires that Widgets have names that begin with lower-case letters,
hence the 'gui_' prefix!
"""

import datetime
import json
import numpy as np
import sys
import tkinter as tk
import yaml

from tkinter           import filedialog, messagebox
from tkintertable      import TableCanvas
from tkgen.gengui      import TkJson
from tkcalendar        import Calendar
from astroquery.simbad import Simbad

def dec2dms (dec, divider=1, separator=':') :
	"""
	Converts a decimal number into a -dd:mm:ss.s string.
	Use divider=15 for R.A.
	"""
	if dec < 0. :
		sn = '-'
	elif divider == 1 :
		sn = '+'
	else :
		sn = ''
	absdec = np.abs(dec)/divider	# divide=15 FOR R.A(deg) -> R.A.(hrs)
	d = int(absdec)
	m = int((absdec-d)*60.)
	s = (absdec-d-m/60.)*3600.
	dms = '{0}{1}{2}{3:02d}{4}{5:.2f}'.format(sn,d,separator,m,separator,s)
	# print (dec,dms,absdec,d,m,s)
	return dms

def dms2dec (dms, multiplier=1.) :
	"""
	Converts a -dd:mm:ss.s string to a decimal number.
	If there are no obvious separation characters, then the
	string is simply converted to float, if possible.
	Use multiplier=15 for converting R.A. to degrees
	"""
	if dms is None or dms == '' :
		raise ValueError ('dms2dec was given empty string!')

	if ':' in dms :
		things = dms.split(':')
	else :
		things = dms.split()
	n = len(things)
	if dms.strip().startswith('-') :
		sg = -1.
	else :
		sg = 1.
	h = float(things[0])
	if n >= 2 :
		m = float(things[1])
	else :
		m = 0.
	if n == 3 :
		s = float(things[2])
	else :
		s = 0.
	dec = sg*(np.abs(h)+m/60.+s/3600.)*multiplier
	# print (dms,dec,sg,h,m,s)
	return dec

class PlanningTool (TkJson) :
	def __init__ (self, jsonfile, yamlfile, *args, **kwargs) :
		title = 'PPPT : pyobs primitive planning tool (Version 0.02)'
		TkJson.__init__ (self,jsonfile,title=title,*args,**kwargs)
		# myFont = tk.font.Font(family="Times New Roman", size=14)

		self.planning_data = None
		self.templates = None
		self.observations = None
		self.times = None
		self.current_template = None

		# GET DEFAULT OBSERVATION
		d = {}
		with open (yamlfile,'r') as stream :
			d = yaml.load(stream)
		if ('default_keys' not in d) or ('default_vals' not in d) :
			messagebox.showinfo ('ERROR','No default observation in YAML file {0}'.format(yamlfile))
			self.exit_app()
		self.default_keys = d['default_keys']
		self.default_vals = d['default_vals']
		self.default_obs = {key:val for key,val in zip(self.default_keys,self.default_vals)}

		# CREATE NOTEBOOKS FROM JSON
		book = self.get ('gui_NOTEBOOK')
		self.notebook (book, './docs/telescope.json',   name='Telescope')
		self.notebook (book, './docs/target.json',	    name='Target')
		self.notebook (book, './docs/observations.json',name='Observations')
		self.notebook (book, './docs/times.json',       name='Times')
		self.notebook (book, './docs/constraints.json', name='Constraints')
		self.notebook (book, './docs/templates.json',   name='My Templates')
		# self.notebook (book, './docs/phasing.json',	name='Phasing')

		# SET UP THE TELESCOPE CLASSES
		self.tel_classes = None
		self.tel_vars = None	# STUPID tk !!!!!!!!
		if 'telescope_classes' in d :
			self.tel_classes = d['telescope_classes']
			self.tel_vars = {}
			for wname,name in self.tel_classes.items() :
				cb = self.checkbox (wname, lambda: self.select_telescope(wname))
				self.tel_vars[wname] = cb

		# SET BEHAVIOR OF THE ELEMENTS
		self.button ('gui_START_BUTTON',lambda: self.calendar_button ('start'))
		self.button ('gui_END_BUTTON',  lambda: self.calendar_button ('end'))
		self.button ('gui_GET_TEMPL_BUTTON', self.get_templates)
		self.button ('gui_TEMPL_BUTTON',     self.query_templates)
		self.button ('gui_SIMBAD_BUTTON',    self.query_simbad)
		self.button ('gui_ADD_OBS_BUTTON',   self.add_observation)
		self.button ('gui_DEL_OBS_BUTTON',   self.delete_observation)
		self.button ('gui_ADD_TIME_BUTTON',  self.add_time)
		self.button ('gui_DEL_TIME_BUTTON',  self.delete_time)
		self.button ('gui_NEW_BUTTON',       self.new_json)
		self.button ('gui_SUBMIT_BUTTON',    self.submit)
		self.button ('gui_LOAD_BUTTON',      self.load_json)
		self.button ('gui_SAVE_BUTTON',      self.save_json)
		self.button ('gui_QUIT_BUTTON',      self.exit_app)
		tree = self.get ('gui_TEMPLATES')
		tree.bind ('<ButtonRelease-1>', self.select_template)

	def select_template (self, evnt=None) :
		""" Stupid Tk: insures that the newest focussed item is selected. """
		tree = self.get ('gui_TEMPLATES')
		idx = tree.focus()
		self.current_template = tree.item (idx)

	def submit (self) :
		messagebox.showinfo ('ERROR','Submissions are not yet implemented!')

	def calendar_button (self, entry_name) :
		top = tk.Tk()	# NEW Tk INSTANCE FOR POPUP
		cal = Calendar (top, font='Arial 14', selectmode='day', cursor='hand1')
		cal.pack (fill='both',expand=True)
		result = None
		def ok () :	# STUPID tkcalendar NEEDS HELP!
			result = cal.selection_get ()
			self.replace_text (entry_name,str(result)+'T00:00:00.0Z')
			top.destroy ()
		but = tk.Button (top,text="OK",command=ok)
		but.pack ()
		top.mainloop()
 
	def create_observations (self) :
		frame = self.get ('gui_OBS_FRAME')
		n = len(self.default_keys)
		self.observations = TableCanvas(frame,rows=0,cols=0)
		self.observations.show ()
		for i in range(n) :
			key = self.default_keys[i]
			self.observations.addColumn (key)
		self.observations.adjustColumnWidths ()
		# self.observations.showtableDefaults ()

	def add_observation (self, obsdict=None) :
		if self.observations is None :
			self.create_observations ()
		if obsdict is None :
			self.observations.addRow (**dict(self.default_obs))
		else :
			self.observations.addRow (**obsdict)
		self.observations.redraw ()

	def delete_observation (self) :
		if self.observations is not None :
			row = self.observations.getSelectedRow ()
			self.observations.deleteRow ()

	def create_times (self) :
		frame = self.get ('gui_TIMES_FRAME')
		self.times = TableCanvas(frame,rows=0,cols=0)
		self.times.show ()
		self.times.addColumn ('start')
		self.times.addColumn ('end')
		self.times.adjustColumnWidths ()
		# self.times.showtableDefaults()

	def add_time (self) :
		if self.times is None :
			self.create_times ()
		tstart = self.get ('start').get()
		tend   = self.get ('end').get()
		d = {'start':tstart,'end':tend}
		self.times.addRow (**d)
		self.times.redraw ()

	def delete_time (self) :
		if self.times is not None :
			row = self.times.getSelectedRow ()
			self.times.deleteRow ()

	def observation_up (self) :
		if self.observations is not None :
			row = self.observations.getSelectedRow ()
			print ('not yet: up row=',row,self.observations.grid_size())

	def observation_down (self) :
		if self.observations is not None :
			row = self.observations.getSelectedRow ()
			print ('not yet: down row=',row,self.observations.grid_size())

	def load_json (self, filename=None) :
		if filename is None or filename == '' :
			filename = filedialog.askopenfilename ()
		if filename is None or len(filename) == 0 :
			return
		with open (filename,'r') as stream :
			self.planning_data = json.load (stream)
		self.json_to_gui ()
		self.replace_text ('gui_CURR_JSON_FILE','current base: '+filename)

	def new_json (self, filename='./docs/lco.json') :
		""" Create an empty LCO-style json planning file. """
		self.load_json (filename)
		self.planning_data['created'] = str(datetime.datetime.now())
		self.json_to_gui ()
		self.replace_text ('gui_CURR_JSON_FILE','current base: '+filename)

	def tofloat (self, s) :
		""" Converts a string to a float.  If empty, 0. is returned. """
		try :
			f = float(s)
		except :
			f = 0.
		return f

	def content_str (self, s) :
		""" Returns either a non-trivial string or a None """
		if s is None or s == '' :
			return None
		else :
			return s

	def gui_to_json (self) :
		""" Places the content in the GUI into the JSON data """
		d = {}
		d['id'] = None
		d['state'] = 'PENDING'
		d['acceptability_threshold'] = None
		if self.planning_data is None or 'created' not in self.planning_data :
			d['created'] = str(datetime.datetime.now())
		else :
			d['created'] = self.planning_data['created']

		# GET THE SELECTED TELESCOPE CLASS
		d['location'] = {'telescope_class': None}
		for wname,intvar in self.tel_vars.items() :
			if intvar.get() :
				d['location'] = {'telescope_class': self.tel_classes[wname]}

		# GET THE CHOSEN TIME-FRAMES
		data = self.times.model.data	# {1:{...}, 2:{...}, ...}
		print (data)
		idx = 1
		windos = []
		while idx in data :
			row = data[idx]
			tstart = row['start']
			tend   = row['end']
			windos.append ({'start':tstart,'end':tend})
			idx += 1
		d['windows'] = windos

		# GET THE GLOBAL TARGET
		name   = self.content_str (self.get ('name').get())
		# print (self.get('ra'),'[{0}]'.format(self.get('ra').get()))
		target_ra     = dms2dec (self.get ('ra').get(), multiplier=15.)
		target_dec    = dms2dec (self.get ('dec').get())
		# target_pm_ra  = self.tofloat(self.get ('proper_motion_ra').get())
		# target_pm_dec = self.tofloat(self.get ('proper_motion_dec').get())
		ra_offset  = 0.	# self.tofloat(self.get('gui_RAOFF_ENTRY' ))/3600.
		dec_offset = 0. # self.tofloat(self.get('gui_DECOFF_ENTRY'))/3600.

		# GET THE GLOBAL CONSTRAINTS
		mxam = self.content_str (self.get ('max_airmass').get())
		mnld = self.content_str (self.get ('min_lunar_distance').get())

		data = self.observations.model.data	# {1:{...}, 2:{...}, ...}
		idx = 1
		confs = []
		duration = 0.0	# SECS
		while idx in data :
			row = data[idx]

			conf = {}

			cfg = {'mode':'full_frame'}
			texp = self.tofloat(row['exp_time'])
			expc = int(row['exp_count'])
			cfg['exposure_time'] = texp
			cfg['exposure_count'] = expc
			duration += 30.+(texp+5.)*expc	# CRUDE ESTIMATE

			cfg['bin_x'] = int(row['bin_x'])
			cfg['bin_y'] = int(row['bin_y'])
			cfg['optical_elements'] = {'filter':row['filter']}
			cfg['rotator_mode'] = None
			cfg['extra_params'] = {'defocus':float(row['defocus'])}

			target = {'name':name}
			target['ra']  = target_ra +ra_offset
			target['dec'] = target_dec+dec_offset
			target['proper_motion_ra']  = 0.	# target_pm_ra
			target['proper_motion_dec'] = 0.	# target_pm_dec
			target['epoch'] = 2000.
			target['parallax'] = 0.
			target['type'] = 'ICRS'
			target['extra_params'] = {}

			conf['id']                 = None
			conf['type']               = self.content_str (row['exp_type']).upper()
			conf['priority']           = int(row['priority'])
			conf['target']             = target
			conf['instrument_type']    = self.content_str (row['detector'])
			conf['instrument_configs'] = [cfg]
			conf['constraints']        = {
				'max_airmass': float(mxam),
				'min_transparency': None,
				'max_lunar_phase': None,
				'max_seeing': None,
				'min_lunar_distance': float(mnld),
				'extra_params': {}
				}
			conf['guiding_config']     = {
				'mode': 'ON',
				'exposure_time': None,
				'optical_elements': {},
				'extra_params': {}
				}
			conf['acquisition_config'] = {
				'mode': 'OFF',
				'exposure_time': None,
				'extra_params': {}
				}

			confs.append(conf)
			idx += 1
		d['configurations'] = confs
		d['duration'] = int(duration)

		d['observation_note'] = ''
		d['modified'] = str(datetime.datetime.now())
		self.planning_data = d

	def json_to_gui (self) :
		d = self.planning_data
		if d is None :
			messagebox.showinfo ('ERROR','No internal planning data!')
			return

		# DELETE ALL CURRENT SEQUENCES
		if self.observations is None :
			self.create_observations ()
		self.observations.model.deleteRows ()

		# CHOOSE TELESCOPE CLASS
		tel_class = d['location']['telescope_class']
		for wname,tel in self.tel_classes.items() :
			widget = self.get (wname)
			if tel_class == tel :
				widget.select()
			else :
				widget.deselect()

		# ONLY USE A SINGLE TARGET SO PARSE THE CONFIGURATIONS AND SEE WHAT HAPPENS !!!
		targ = None

		# FOR ALL CONFIGURATIONS ...
		for conf in d['configurations'] :
			prio      = conf['priority']					# int
			objtype   = conf['type']
			inst_type = conf['instrument_type']

			inst    = conf['instrument_configs'][0]
			exptime = inst['exposure_time']					# float
			count   = inst['exposure_count']				# int
			filt    = inst['optical_elements']['filter']
			binx    = inst['bin_x']							# int
			biny    = inst['bin_y']							# int
			defocus = inst['extra_params']['defocus']		# float

			obs = {}
			obs['detector']  = inst_type
			obs['exp_type']  = objtype
			obs['exp_time']  = '{0:.3f}'.format(exptime)
			obs['exp_count'] = str(count)
			obs['filter']    = filt
			obs['defocus']   = '{0:.3f}'.format(defocus)
			obs['priority']  = str(prio)
			obs['bin_x']     = str(binx)
			obs['bin_y']     = str(biny)

			# REPLACE POSITION DATA COMMON TO ALL SEQUENCES
			if targ is None and 'target' in conf :
				targ  = conf['target']
				name  = targ['name']
				ra    = targ['ra']		# float degrees!
				dec   = targ['dec']		# float degrees!
				pmra  = '{0:.3f}'.format(targ['proper_motion_ra'])
				pmdec = '{0:.3f}'.format(targ['proper_motion_dec'])
				self.replace_text ('name',name)
				self.replace_text ('ra',dec2dms(ra,divider=15.))
				self.replace_text ('dec',dec2dms(dec))
				# self.replace_text ('proper_motion_ra',pmra)
				# self.replace_text ('proper_motion_dec',pmdec)
				# obs['ra_offset']  = '0.0'
				# obs['dec_offset'] = '0.0'
			else :
				dra  = (ra-targ['ra'])*3600.	# arcsec
				ddec = (dec-targ['dec'])*3600.	# arcsec
				# obs['ra_offset']  = str(dra)
				# obs['dec_offset'] = str(ddec)
			self.add_observation (obs)

			# USE LAST INDIVIDUAL CONSTRAINT
			cnstr = conf['constraints']
			mx_airmass = '{0:.2f}'.format(cnstr['max_airmass'])
			mn_lunar = '{0:.2f}'.format(cnstr['min_lunar_distance'])
			self.replace_text ('max_airmass',mx_airmass)
			self.replace_text ('min_lunar_distance',mn_lunar)

		# ENTER WINDOW (USE LAST)
		for windows in d['windows'] :
			self.replace_text ('start',windows['start'])
			self.replace_text ('end',windows['end'])

	def save_json (self) :
		self.gui_to_json ()
		filename = filedialog.asksaveasfilename (initialdir='.',title='Select file')
		if filename is None or len(filename) == 0 :
			return
		with open (filename,'w') as stream :
			json.dump (self.planning_data,stream)
		self.replace_text ('gui_CURR_JSON_FILE','current base: '+filename)

	def replace_text (self, label, newtext) :
		x = self.get (label)
		if isinstance (x,tk.Label) :
			x.config (state='normal')
			x.config (text=newtext)	# x.configure (text=newtext)
			x.config (state='disabled')
		else :
			x.delete (0,tk.END)
			x.insert (tk.END,newtext)

	def get_templates (self) :
		self.templates = None
		# READ FROM FILE
		filename = filedialog.askopenfilename (initialdir='.',title='Select file')
		if filename is None or len(filename) == 0 :
			return
		try :
			with open (filename,'r') as stream :
				self.templates = yaml.load (stream)
		except :
			messagebox.showinfo ('ERROR','Unable to open the user template"{0}"'.format(filename))
			return

		tree = self.get ('gui_TEMPLATES')
		tree.heading ('#0',text='Target')
		tree.heading ('0',text='Key')
		tree.heading ('1',text='Value')
		for key,d in self.templates.items() :
			item = self.treeview (tree,key,['',''])
			for dkey,val in d.items() :
				self.treeview (tree,'',[dkey,str(val)],parent=item)

	def query_templates (self) :
		if self.templates is None :
			self.get_templates ()
		if self.templates is None :
			return

		# GET CATALOGUE ENTRY
		targetname = self.get ('name')
		objname = targetname.get ()
		if objname not in self.templates :
			self.select_template ()
			d = self.current_template
			if (d is None) or ('text' not in d) or (d['text'] == '') :
				messagebox.showinfo ('ERROR!','Please first select the template')
				return
			objname = d['text']
		if objname in self.templates :
			d = self.templates[objname]
			if ('ra' not in d) and ('dec' not in d) :
				self.replace_text ('name',objname)
				self.query_simbad ()
			self.parse_templates_entry (d) 
		else :
			messagebox.showinfo ('ERROR!','Unable to find "{0}" in your templates!'.format(objname))
		self.replace_text ('gui_CURR_JSON_FILE','current base: template for '+objname)

	def parse_templates_entry (self, objd) :
		for key,val in objd.items() :
			try :
				x = self.get (key)
				self.replace_text (key,str(val))
			except :
				print ('whoops!',key,val,x)
				pass

	def query_simbad (self) :
		objname = self.get ('name').get()
		if objname is None or objname == '' :
			messagebox.showinfo ('ERROR','No target name given!')
			return
		sim = Simbad()
		sim.add_votable_fields ('pmra','pmdec')
		tab = sim.query_object (objname)
		if tab is not None and len(tab) > 0 :
			row = tab[0]
			label = row['MAIN_ID']
			ra = row['RA']
			dec = row['DEC']
			pmra = row['PMRA']
			pmdec = row['PMDEC']
			print ('Note PM for',label,ra,dec,' : ',pmra,pmdec,' : and add to JSON afterwards!')
			self.replace_text ('name',label)
			self.replace_text ('ra',  ra)
			self.replace_text ('dec', dec)
			# self.replace_text ('proper_motion_ra',  pmra)
			# self.replace_text ('proper_motion_dec', pmdec)
		else :
			messagebox.showinfo ('ERROR','SIMBAD error!')

	def select_telescope (self, wname) :
		# print ('selecting telescope',wname)
		for key in self.tel_classes :	# FOR EVERY WIDGET NAME
			if self.tel_classes[key] != wname :
				other = self.get (key)
				other.deselect ()
		other = self.get (wname)
		other.select ()
 
	def exit_app (self) :
		self.destroy ()

if __name__ == '__main__' :
	root = PlanningTool ('./docs/pppt.json','./docs/default_observation.yaml')
	root.mainloop()
